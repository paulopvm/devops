// This step should not normally be used in your script. Consult the inline help for details.
podTemplate(
    containers: [
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', livenessProbe: containerLivenessProbe(execArgs: '', failureThreshold: 0, initialDelaySeconds: 0, periodSeconds: 0, successThreshold: 0, timeoutSeconds: 0), name: 'docker-container', resourceLimitCpu: '', resourceLimitMemory: '', resourceRequestCpu: '', resourceRequestMemory: '', ttyEnabled: true),
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.11.0', name: 'helm-container', ttyEnabled: true)        
        ], 
    label: 'questcode', 
    name: 'questcode', 
    namespace: 'devops', 
    volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')], 
)

{
    //Start Pipeline
    node('questcode') {
        def REPOS
        def IMAGE_VERSION
        def IMAGE_NAME = "frontend"
        def ENV = "staging"

        stage('Checkout') {
            echo 'Iniciando Clone do Repositorio'
            REPOS = git credentialsId: 'bitbucket', url: 'git@bitbucket.org:questcodeclass/frontend.git'
            IMAGE_VERSION = sh label: '', returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim()
        }
        stage('Package') {
            container('docker-container') {

                echo 'Iniciando empacotamento com Docker'
                withCredentials( //Necessario credencias para acessar o docker
                    [usernamePassword(
                        credentialsId: 'dockerhub', 
                        passwordVariable: 'DOCKER_HUB_PASSWORD', 
                        usernameVariable: 'DOCKER_HUB_USER'
                        )
                    ]
                ) 
                {
                    sh label: '', script: "docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}"
                    sh label: '', script: "docker build -t ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION} . --build-arg NPM_ENV='${ENV}'"
                    sh label: '', script: "docker push ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION}"
                }
            }
            
        }
        stage('Deploy') {
            container('helm-container') {
                echo 'Iniciando Deploy com Helm'
                sh label: '', script: "helm init --client-only"
                sh label: '', script: "helm repo add questcode http://helm-chartmuseum:8080"
                sh label: '', script: "helm repo update"
                sh label: '', script: "helm upgrade staging-frontend questcode/frontend --set image.tag=${IMAGE_VERSION}"
            }
        }
    } // end of node
}